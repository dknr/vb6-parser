namespace Vb6Parser.Models
{
    public class Form
    {
        public string VbName { get; set; }
        public Control RootControl { get; set; }
    }
}