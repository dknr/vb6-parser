using System.Collections.Generic;

namespace Vb6Parser.Models
{
    public class Control
    {
        public string Type { get; set; }
        public List<KeyValuePair<string, string>> Properties { get; set; }
        public List<Control> Children { get; set; }
    }
}