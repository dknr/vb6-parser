using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Antlr4.Runtime;
using Antlr4.Runtime.Tree;
using Vb6Parser.Models;
using Vb6Parser.Visitors.Form;

namespace Vb6Parser
{
    public static class ParserExtensions
    {
        public static T ParseWithVisitor<T>(this TextReader textReader, IParseTreeVisitor<T> visitor)
        {
            var inputStream = new AntlrInputStream(textReader);

            var lexer = new VisualBasic6Lexer(inputStream);
            var commonTokenStream = new CommonTokenStream(lexer);
            var parser = new VisualBasic6Parser(commonTokenStream);

            return visitor.Visit(parser.startRule());
        }

        public static T ParseFile<T>(this string filePath, IParseTreeVisitor<T> visitor)
        {
            using var textReader = new StreamReader(filePath);
            return textReader.ParseWithVisitor(visitor);
        }

        public static Form ParseFileAsForm(this string filePath)
            => filePath.ParseFile(new FormVisitor());

        public static List<T> VisitCollection<T>(this IEnumerable<IParseTree> self, Func<IParseTree, T> visit)
            => self
                .Select(visit)
                .Where(p => p != null)
                .ToList();

        public static List<T> VisitCollection<T>(this IEnumerable<IParseTree> self, Func<IParseTree, T?> visit)
            where T : struct
            => self
                .Select(visit)
                .Where(p => p != null)
                .OfType<T>()
                .ToList();
    }
}