namespace Vb6Parser.Visitors.Form
{
    public class FormVisitor : VisualBasic6BaseVisitor<Models.Form>
    {
        public override Models.Form VisitStartRule(VisualBasic6Parser.StartRuleContext context)
        {
            return Visit(context.module());
        }

        public override Models.Form VisitModule(VisualBasic6Parser.ModuleContext context)
        {
            return new Models.Form
            {
                RootControl = new ControlTreeVisitor().Visit(context.controlProperties()),
            };
        }
    }
}
