using System.Collections.Generic;

namespace Vb6Parser.Visitors.Form
{
    public class ControlPropertyVisitor : VisualBasic6BaseVisitor<KeyValuePair<string, string>?>
    {
        public override KeyValuePair<string, string>? VisitCp_SingleProperty(VisualBasic6Parser.Cp_SinglePropertyContext context)
        {
            return new KeyValuePair<string, string>(
                context.implicitCallStmt_InStmt().GetText(),
                context.cp_PropertyValue().GetText()
            );
        }
    }
}