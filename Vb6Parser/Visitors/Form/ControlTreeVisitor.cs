using Vb6Parser.Models;

namespace Vb6Parser.Visitors.Form
{
    public class ControlTreeVisitor : VisualBasic6BaseVisitor<Control>
    {
        public override Control VisitControlProperties(VisualBasic6Parser.ControlPropertiesContext context)
        {
            var controlPropertyVisitor = new ControlPropertyVisitor();
            
            return new Control
            {
                Type = context.cp_ControlType().GetText(),
                Properties = context.cp_Properties().VisitCollection(controlPropertyVisitor.Visit),
                Children = context.cp_Properties().VisitCollection(Visit),
            };
        }
    }
}