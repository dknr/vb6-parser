using System.Linq;
using Vb6Parser.Models;
using Xunit;

namespace Vb6Parser.Tests
{
    public class FormVisitorTests
    {
        private Form GetTestForm() => "/Users/dknr/src/old/limited-source/aria/client/frmAdmin.frm".ParseFileAsForm();

        [Fact]
        public void CanParseRootControl()
        {
            var form = GetTestForm();
            var rootControl = form.RootControl;
            Assert.Equal("VB.Form", rootControl.Type);
            
            var properties = rootControl.Properties;
            Assert.NotEmpty(properties);
            Assert.All(properties.Select(p => p.Key), Assert.NotNull);
            Assert.All(properties.Select(p => p.Value), Assert.NotNull);
            
            var caption = properties.Single(p => p.Key == "Caption");
            Assert.Equal("\"Administration Panel\"", caption.Value);
        }

        [Fact]
        public void CanParseControlTree()
        {
            var form = GetTestForm();
            var children = form.RootControl.Children;
            
            Assert.NotEmpty(children);
            Assert.All(children, Assert.NotNull);

            var firstChild = children.First();
            Assert.Equal("VB.Frame", firstChild.Type);
            var firstChildCaption = firstChild.Properties.Single(p => p.Key == "Caption");
            Assert.Equal("\"Weather Commands\"", firstChildCaption.Value);

            var firstGrandchildren = firstChild.Children;
            Assert.NotEmpty(firstGrandchildren);
            Assert.All(children, Assert.NotNull);

            var firstGrandchild = firstGrandchildren.First();
            Assert.Equal("VB.CommandButton", firstGrandchild.Type);
            var firstGrandchildCaption = firstGrandchild.Properties.Single(p => p.Key == "Caption");
            Assert.Equal("\"Thunder\"", firstGrandchildCaption.Value);
        }
    }
}